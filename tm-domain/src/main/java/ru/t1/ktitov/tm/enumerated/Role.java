package ru.t1.ktitov.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum Role {

    USUAL("Usual user"),
    ADMIN("Administrator");

    @NotNull
    private final String displayName;

    Role(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @Nullable
    public static Role toRole(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@Nullable final Role role : values()) {
            if (role == null) continue;
            if (role.getDisplayName().equals(value)) return role;
        }
        return null;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
