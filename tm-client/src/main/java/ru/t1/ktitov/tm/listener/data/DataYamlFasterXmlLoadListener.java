package ru.t1.ktitov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.dto.request.data.DataYamlFasterXmlLoadRequest;
import ru.t1.ktitov.tm.event.ConsoleEvent;

@Component
public class DataYamlFasterXmlLoadListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-load-yaml-fasterxml";

    @NotNull
    public static final String DESCRIPTION = "Load data from yaml file by fasterxml";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataYamlFasterXmlLoadListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull DataYamlFasterXmlLoadRequest request = new DataYamlFasterXmlLoadRequest(getToken());
        domainEndpoint.loadDataYamlFasterXml(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
