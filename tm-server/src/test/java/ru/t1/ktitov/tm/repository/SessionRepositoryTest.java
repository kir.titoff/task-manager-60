package ru.t1.ktitov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.api.repository.model.ISessionRepository;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.api.service.model.IUserService;
import ru.t1.ktitov.tm.marker.UnitCategory;
import ru.t1.ktitov.tm.model.Session;
import ru.t1.ktitov.tm.repository.model.SessionRepository;
import ru.t1.ktitov.tm.service.model.UserService;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.t1.ktitov.tm.constant.SessionTestData.*;
import static ru.t1.ktitov.tm.constant.UserTestData.*;

@Component
@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private EntityManager entityManager;

    @NotNull
    protected ISessionRepository getRepository() {
        return context.getBean(SessionRepository.class);
    }

    private void compareSessions(@NotNull final Session session1, @NotNull final Session session2) {
        Assert.assertEquals(session1.getId(), session2.getId());
        Assert.assertEquals(session1.getUser().getId(), session2.getUser().getId());
        Assert.assertEquals(session1.getRole(), session2.getRole());
    }

    private void compareSessions(
            @NotNull final List<Session> sessionList1,
            @NotNull final List<Session> sessionList2) {
        Assert.assertEquals(sessionList1.size(), sessionList2.size());
        for (int i = 0; i < sessionList1.size(); i++) {
            compareSessions(sessionList1.get(i), sessionList2.get(i));
        }
    }

    @Before
    public void initData() {
        @NotNull final IUserService userService = new UserService();
        userService.add(USER1);
        userService.add(USER2);
        userService.add(ADMIN3);
    }

    @After
    public void tearDown() {
        @NotNull final IUserService userService = new UserService();
        userService.removeById(USER1.getId());
        userService.removeById(USER2.getId());
        userService.removeById(ADMIN3.getId());
    }

    @Test
    @SneakyThrows
    public void add() {
        try {
            @NotNull final ISessionRepository repository = getRepository();
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_SESSION1);
            compareSessions(USER1_SESSION1, repository.findAll().get(0));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void addList() {
        try {
            @NotNull final ISessionRepository repository = getRepository();
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_SESSION1);
            repository.add(USER1_SESSION2);
            repository.add(USER1_SESSION3);
            Assert.assertEquals(3, repository.findAll().size());
            compareSessions(USER1_SESSION_LIST, repository.findAll());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void clear() {
        try {
            @NotNull final ISessionRepository repository = getRepository();
            entityManager.getTransaction().begin();
            repository.add(USER1_SESSION1);
            compareSessions(USER1_SESSION1, repository.findAll().get(0));
            repository.clear();
            Assert.assertTrue(repository.findAll().isEmpty());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void clearByUserId() {
        try {
            @NotNull final ISessionRepository repository = getRepository();
            entityManager.getTransaction().begin();
            repository.add(USER1_SESSION1);
            repository.add(USER1_SESSION2);
            repository.add(USER1_SESSION3);
            compareSessions(USER1_SESSION_LIST, repository.findAll());
            repository.clear(USER2.getId());
            Assert.assertFalse(repository.findAll().isEmpty());
            repository.clear(USER1.getId());
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_SESSION1);
            repository.add(USER2_SESSION1);
            repository.clear(USER1.getId());
            compareSessions(USER2_SESSION1, repository.findAll().get(0));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAll() {
        try {
            @NotNull final ISessionRepository repository = getRepository();
            entityManager.getTransaction().begin();
            repository.add(USER1_SESSION1);
            repository.add(USER1_SESSION2);
            repository.add(USER1_SESSION3);
            repository.add(USER2_SESSION1);
            repository.add(USER2_SESSION2);
            repository.add(USER2_SESSION3);
            Assert.assertEquals(6, repository.findAll().size());
            compareSessions(USER1_SESSION_LIST, repository.findAll(USER1.getId()));
            compareSessions(USER2_SESSION_LIST, repository.findAll(USER2.getId()));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        try {
            @NotNull final ISessionRepository repository = getRepository();
            entityManager.getTransaction().begin();
            repository.add(USER1_SESSION1);
            repository.add(USER1_SESSION2);
            repository.add(USER1_SESSION3);
            compareSessions(USER1_SESSION1, repository.findOneById(USER1_SESSION1.getId()));
            Assert.assertNull(repository.findOneById(USER2.getId(), USER1_SESSION1.getId()));
            compareSessions(USER1_SESSION1, repository.findOneById(USER1.getId(), USER1_SESSION1.getId()));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void remove() {
        try {
            @NotNull final ISessionRepository repository = getRepository();
            entityManager.getTransaction().begin();
            repository.add(USER1_SESSION1);
            repository.add(USER1_SESSION2);
            repository.add(USER1_SESSION3);
            repository.remove(USER1_SESSION1);
            Assert.assertEquals(2, repository.findAll().size());
            repository.removeById(USER1_SESSION2.getId());
            Assert.assertEquals(1, repository.findAll().size());
            compareSessions(USER1_SESSION3, repository.findAll().get(0));
            repository.clear();
            repository.add(USER1_SESSION1);
            repository.add(USER1_SESSION2);
            repository.add(USER1_SESSION3);
            Assert.assertEquals(3, repository.findAll().size());
            repository.removeById(USER2.getId(), USER1_SESSION1.getId());
            Assert.assertEquals(3, repository.findAll().size());
            repository.removeById(USER1.getId(), USER1_SESSION1.getId());
            repository.removeById(USER1.getId(), USER1_SESSION2.getId());
            compareSessions(USER1_SESSION3, repository.findAll().get(0));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
