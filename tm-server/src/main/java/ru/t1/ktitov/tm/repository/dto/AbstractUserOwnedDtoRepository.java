package ru.t1.ktitov.tm.repository.dto;

import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.ktitov.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.ktitov.tm.dto.model.AbstractUserOwnedModelDTO;

@Repository
@Scope("prototype")
public abstract class AbstractUserOwnedDtoRepository<M extends AbstractUserOwnedModelDTO>
        extends AbstractDtoRepository<M> implements IUserOwnedDtoRepository<M> {

    @Nullable
    @Override
    public M add(
            @Nullable final String userId,
            @Nullable final M model
    ) {
        if (userId == null) return null;
        if (model == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Nullable
    @Override
    public M removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || id == null) return null;
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    @Override
    public M remove(
            @Nullable final String userId,
            @Nullable final M model
    ) {
        if (userId == null || model == null) return null;
        entityManager.remove(model);
        return model;
    }

}
