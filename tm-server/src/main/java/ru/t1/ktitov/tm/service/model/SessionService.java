package ru.t1.ktitov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ktitov.tm.api.repository.model.ISessionRepository;
import ru.t1.ktitov.tm.api.service.model.ISessionService;
import ru.t1.ktitov.tm.model.Session;
import ru.t1.ktitov.tm.repository.model.SessionRepository;

@Service
public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository>
        implements ISessionService {


    @NotNull
    @Autowired
    private ISessionRepository sessionRepository;

}
