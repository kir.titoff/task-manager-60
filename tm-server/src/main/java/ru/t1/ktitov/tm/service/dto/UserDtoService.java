package ru.t1.ktitov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ktitov.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.api.service.dto.IUserDtoService;
import ru.t1.ktitov.tm.enumerated.Role;
import ru.t1.ktitov.tm.exception.field.*;
import ru.t1.ktitov.tm.exception.user.UserEmailExistsException;
import ru.t1.ktitov.tm.exception.user.UserLoginExistsException;
import ru.t1.ktitov.tm.exception.entity.UserNotFoundException;
import ru.t1.ktitov.tm.dto.model.UserDTO;
import ru.t1.ktitov.tm.repository.dto.UserDtoRepository;
import ru.t1.ktitov.tm.util.HashUtil;

import javax.transaction.Transactional;

@Service
public final class UserDtoService extends AbstractDtoService<UserDTO, UserDtoRepository> implements IUserDtoService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IUserDtoRepository repository;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new UserLoginExistsException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        repository.add(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new UserLoginExistsException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (isEmailExist(email)) throw new UserEmailExistsException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        repository.add(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new UserLoginExistsException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        repository.add(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final UserDTO user = findOneById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        repository.update(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final UserDTO user = findOneById(id);
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null) throw new EmptyPasswordException();
        user.setPasswordHash(passwordHash);
        repository.update(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        repository.update(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        repository.update(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return remove(user);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new UserNotFoundException();
        @Nullable final UserDTO user = findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return remove(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return repository.findByLogin(login);
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return repository.findByEmail(email);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return repository.findByLogin(login) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return repository.findByEmail(email) != null;
    }

}
