package ru.t1.ktitov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.ktitov.tm.api.repository.model.IUserRepository;
import ru.t1.ktitov.tm.model.User;

import java.util.List;

@Repository
@Scope("prototype")
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public @NotNull List<User> findAll() {
        @NotNull final String jpql = "SELECT m FROM User m";
        return entityManager.createQuery(jpql, User.class)
                .setHint("org.hibernate.cacheable", true)
                .getResultList();
    }

    @Override
    public User findOneById(@Nullable final String id) {
        return entityManager.find(User.class, id);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        @NotNull final String jpql = "SELECT m FROM User m WHERE login = :login";
        return entityManager.createQuery(jpql, User.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) return null;
        @NotNull final String jpql = "SELECT m FROM User m WHERE email = :email";
        return entityManager.createQuery(jpql, User.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Override
    public void clear() {
        @NotNull final List<User> users = findAll();
        if (!users.isEmpty())
            users.forEach(entityManager::remove);
    }

}
