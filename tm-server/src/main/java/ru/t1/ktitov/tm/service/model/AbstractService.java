package ru.t1.ktitov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.ktitov.tm.api.repository.model.IRepository;
import ru.t1.ktitov.tm.api.service.model.IService;
import ru.t1.ktitov.tm.exception.entity.EntityNotFoundException;
import ru.t1.ktitov.tm.exception.field.EmptyIdException;
import ru.t1.ktitov.tm.model.AbstractModel;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;

@Service
public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    @Autowired
    protected IRepository<M> repository;

    @Nullable
    @SneakyThrows
    @Transactional
    public M add(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        repository.add(model);
        return model;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Collection<M> add(@Nullable final Collection<M> models) {
        if (models == null) throw new EntityNotFoundException();
        models.forEach(repository::add);
        return models;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Collection<M> set(@Nullable final Collection<M> models) {
        if (models == null) throw new EntityNotFoundException();
        repository.clear();
        models.forEach(repository::add);
        return models;
    }

    @NotNull
    @Override
    @Transactional
    public M update(@NotNull final M model) {
        repository.update(model);
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final M model = repository.findOneById(id);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.findOneById(id) != null;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public M remove(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        repository.remove(model);
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public M removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final M model = repository.findOneById(id);
        if (model == null) throw new EntityNotFoundException();
        repository.removeById(id);
        return model;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        repository.clear();
    }

}
