package ru.t1.ktitov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.ktitov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.ktitov.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.ktitov.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.ktitov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.ktitov.tm.exception.entity.TaskNotFoundException;
import ru.t1.ktitov.tm.exception.field.EmptyProjectIdException;
import ru.t1.ktitov.tm.exception.field.EmptyTaskIdException;
import ru.t1.ktitov.tm.exception.field.EmptyUserIdException;
import ru.t1.ktitov.tm.dto.model.ProjectDTO;
import ru.t1.ktitov.tm.dto.model.TaskDTO;
import ru.t1.ktitov.tm.repository.dto.ProjectDtoRepository;
import ru.t1.ktitov.tm.repository.dto.TaskDtoRepository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@Service
public final class ProjectTaskDtoService implements IProjectTaskDtoService {

    @NotNull
    @Autowired
    private IProjectDtoRepository projectRepository;

    @NotNull
    @Autowired
    private ITaskDtoRepository taskRepository;

    @Override
    @SneakyThrows
    @Transactional
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyTaskIdException();
        if (projectRepository.findOneById(userId, projectId) == null) throw new ProjectNotFoundException();
        @Nullable final TaskDTO task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        taskRepository.update(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyTaskIdException();
        if (projectRepository.findOneById(userId, projectId) == null) throw new ProjectNotFoundException();
        @Nullable final TaskDTO task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        taskRepository.update(task);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();
        if (projectRepository.findOneById(userId, projectId) == null) throw new ProjectNotFoundException();
        @NotNull final List<TaskDTO> tasks = taskRepository.findAllByProjectId(userId, projectId);
        for (final TaskDTO task : tasks) taskRepository.removeById(task.getId());
        @Nullable final ProjectDTO project = projectRepository.findOneById(userId, projectId);
        projectRepository.removeById(userId, projectId);
        return project;
    }

}
