package ru.t1.ktitov.tm.configuration;

import org.hibernate.cfg.Environment;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.internal.SessionFactoryImpl;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.listener.EventListener;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.t1.ktitov.tm")
public class ServerConfiguration {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private EventListener eventListener;

    @Bean
    @NotNull
    public DataSource dataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(propertyService.getDatabaseDriver());
        dataSource.setUrl(propertyService.getDatabaseUrl());
        dataSource.setUsername(propertyService.getDatabaseUser());
        dataSource.setPassword(propertyService.getDatabasePassword());
        return dataSource;
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(
            @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactoryBean
    ) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactoryBean.getObject());
        initListeners(entityManagerFactoryBean.getObject());
        return transactionManager;
    }

    @Bean
    @NotNull
    LocalContainerEntityManagerFactoryBean entityManagerFactory(@NotNull final DataSource dataSource) {
        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.t1.ktitov.tm.dto.model", "ru.t1.ktitov.tm.model");
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.DRIVER, propertyService.getDatabaseDriver());
        properties.put(Environment.URL, propertyService.getDatabaseUrl());
        properties.put(Environment.USER, propertyService.getDatabaseUser());
        properties.put(Environment.PASS, propertyService.getDatabasePassword());
        properties.put(Environment.DIALECT, propertyService.getDatabaseDialect());
        properties.put(Environment.HBM2DDL_AUTO, propertyService.getDatabaseHbm2ddlAuto());
        properties.put(Environment.SHOW_SQL, propertyService.getDatabaseShowSql());
        properties.put(Environment.FORMAT_SQL, propertyService.getDatabaseFormatSql());
        properties.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getSecondLvlCache());
        properties.put(Environment.CACHE_REGION_FACTORY, propertyService.getFactoryClass());
        properties.put(Environment.USE_QUERY_CACHE, propertyService.getUseQueryCache());
        properties.put(Environment.USE_MINIMAL_PUTS, propertyService.getUseMinimalPuts());
        properties.put(Environment.CACHE_REGION_PREFIX, propertyService.getRegionPrefix());
        properties.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getProviderFilePath());
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    private void initListeners(@NotNull final EntityManagerFactory entityManagerFactory) {
        @NotNull final SessionFactoryImpl sessionFactory =
                entityManagerFactory.unwrap(SessionFactoryImpl.class);
        @NotNull final EventListenerRegistry registryListener =
                sessionFactory.getServiceRegistry().getService(EventListenerRegistry.class);
        registryListener.getEventListenerGroup(EventType.POST_INSERT).appendListener(eventListener);
        registryListener.getEventListenerGroup(EventType.POST_DELETE).appendListener(eventListener);
        registryListener.getEventListenerGroup(EventType.POST_UPDATE).appendListener(eventListener);
    }

}
